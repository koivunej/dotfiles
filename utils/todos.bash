#!/bin/bash

git --no-pager diff -U0 main | egrep '^+.*(TODO|FIXME)' | sed 's/^+//' | git --no-pager grep -nFf - 2>/dev/null | sed -Ee 's/^(\S+:)\s*/\1\t/'| column -t -s$'\t'
