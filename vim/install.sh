#!/bin/sh

scripts="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd )"

if ! which vim &>/dev/null; then
    echo "install.sh: vim not found" >&2
    exit 1
fi

mkdir -pv "$HOME/.vim/bundle"

neobundle="$HOME/.vim/bundle/neobundle.vim"

if [ -d "$neobundle" ]; then
    if ! [ -d "$neobundle/.git" ]; then
        echo "install.sh: not a git repo: $neobundle/.git" >&2
        exit 1
    fi
    (cd "$neobundle" && git pull --ff-only --quiet) || exit 1
else
    git clone --quiet "https://github.com/Shougo/neobundle.vim" "$neobundle"
fi

ln -sfv "$scripts/vimrc" ~/.vimrc

vim +NeoBundleInstall +qall
