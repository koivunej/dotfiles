#!/bin/sh

# pretty much adapted from webpro/dotfiles/install.sh

scripts="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd )"

mkdir -pv "$HOME/.shell.d"

ln -sfv "$scripts/.profile" ~
ln -sfv "$scripts/.bashrc" ~
ln -sfv "$scripts/.bash_profile" ~
ln -sfv "$scripts/.editorconfig" ~
ln -sfv "$scripts/.tmux.conf" ~
ln -sfv "$scripts/.inputrc" ~

if [ -L "$HOME/.input.rc" ] && [ "$(readlink -f "$HOME/.input.rc")" = "$scripts/.input.rc" ]; then
    # fix old typo..
    rm -v "$HOME/.input.rc"
fi

ln -sfv "$scripts/.gitconfig" ~
ln -sfv "$scripts/.githelpers" ~

ln -sfv "$scripts/hg/hgrc" ~/.hgrc
ln -sfv "$scripts/hg/hghelpers" ~/.shell.d/
ln -sfv "$scripts/utils/recover_ssh-agent" ~/.shell.d/
ln -sfv "$scripts/utils/sensible.bash" ~/.shell.d/

# there might be some scripts which attempt to put this into PATH
mkdir -pv "$HOME/.local/bin"
