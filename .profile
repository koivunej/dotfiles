# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.

export DOTFILES="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd )"

. "$DOTFILES/sh/env"
. "$DOTFILES/sh/login"
